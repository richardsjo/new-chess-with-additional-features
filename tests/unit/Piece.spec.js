// import * as test from '@vue/test-utils'
import {mount} from '@vue/test-utils'
import Piece from '@/components/Piece.vue'


describe('Piece', () => {

const wrapper = mount(Piece,{propsData: {
  character: {player:true,style:'border:1px solid black'}
}})
wrapper.setData({
  info:null
})

it('shows the grass', () => {
  expect(wrapper.contains('.grassImg')).toBe(true)
})

it('returns null when there is no information for the piece', () => {
  expect(wrapper.vm.cellInfo()).toBe(null)
})

it('returns information about the piece if it has some', () => {
  wrapper.setData({
    info: {
      image: "crossed-swords",
      archetype: {
        name: 'Soldier',
        health: 80,
        movement: 2,
        player: null,
        physicalDefence: 10,
        magicalDefence: 4,
        range: 1,
        damage: 27,
        movementToken: 2,
        damageType: 'phys',
      },
      hasMoved: false,
      style: 'border:1px solid black',
      hasAttacked: false
    },
    baseHP: 80
  })
  expect(wrapper.vm.cellInfo()).toEqual([expect.stringContaining(wrapper.vm.info.archetype.name),expect.stringContaining(String(wrapper.vm.info.archetype.health)),expect.stringContaining(String(wrapper.vm.info.archetype.physicalDefence)),expect.stringContaining(String(wrapper.vm.info.archetype.magicalDefence)),expect.stringContaining(String(wrapper.vm.info.archetype.range)),expect.stringContaining(String(wrapper.vm.info.archetype.damage)),wrapper.vm.info.archetype])
})

it('returns the information about the piece', () => {
  expect(wrapper.vm.getInfoData()).toEqual(wrapper.vm.info)
})

it('returns the correct amount of movement possibilities for the given information', () => {
  wrapper.vm.info={
    image: "high-shot",
    archetype: {
      name: 'Archer',
      health: 60,
      movement: 1,
      player: null,
      physicalDefence: 5,
      magicalDefence: 3,
      range: 3,
      damage: 35,
      movementToken: 1,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  expect(wrapper.vm.move(4,4)).toEqual(expect.arrayContaining([{xPosition:3,yPosition:4,movesUsed:1},{xPosition:4,yPosition:4,movesUsed:0},{xPosition:4,yPosition:3,movesUsed:1},{xPosition:4,yPosition:5,movesUsed:1},{xPosition:5,yPosition:4,movesUsed:1}]))
})

it('returns the correct number of attack possibilities for the given information', () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 80,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 2,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  expect(wrapper.vm.attack(4,4)).toEqual([[{xPosition:3,yPosition:4}],[{xPosition:4,yPosition:3}],[{xPosition:4,yPosition:4}],[{xPosition:4,yPosition:5}],[{xPosition:5,yPosition:4}]])
})

it('changes the health of the piece by the correct amount', () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 80,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 2,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  wrapper.vm.inflictDamage({
    image: "high-shot",
    archetype: {
      name: 'Archer',
      health: 60,
      movement: 1,
      player: null,
      physicalDefence: 5,
      magicalDefence: 3,
      range: 3,
      damage: 35,
      movementToken: 1,
      damageType: 'phys',
    }
  })
    expect(wrapper.vm.info.archetype.health).toEqual(55)
})

it ('returns the correct amount of health lost by the piece', () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 80,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 2,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  expect(wrapper.vm.inflictDamage({
    image: "high-shot",
    archetype: {
      name: 'Archer',
      health: 60,
      movement: 1,
      player: null,
      physicalDefence: 5,
      magicalDefence: 3,
      range: 3,
      damage: 35,
      movementToken: 1,
      damageType: 'phys',
    }
  })).toEqual(25)
})

it('changes whether the piece has attacked or not', () => {
  wrapper.vm.changeHasAttacked()
  expect(wrapper.vm.info.hasAttacked).toBe(true)
})

it('increases the health of the piece by the correct amount', () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 40,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 2,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  wrapper.vm.baseHP=80
  wrapper.vm.heal({
    image: "pope-crown",
    archetype: {
      name: 'Bishop',
      health: 50,
      movement: 1,
      player: null,
      physicalDefence: 2,
      magicalDefence: 10,
      range: 1,
      damage: 30,
      movementToken: 1,
      damageType: 'mag',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  })
  expect(wrapper.vm.info.archetype.health).toEqual(70)
})

it("doesn't increase the health of the piece beyond its maximum", () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 60,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 2,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  wrapper.vm.baseHP=80
  wrapper.vm.heal({
    image: "pope-crown",
    archetype: {
      name: 'Bishop',
      health: 50,
      movement: 1,
      player: null,
      physicalDefence: 2,
      magicalDefence: 10,
      range: 1,
      damage: 30,
      movementToken: 1,
      damageType: 'mag',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  })
  expect(wrapper.vm.info.archetype.health).toEqual(80)
})

it('returns the amount healed by the piece', () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 40,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 2,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  }
  wrapper.vm.baseHP=80
  expect(wrapper.vm.heal({
    image: "pope-crown",
    archetype: {
      name: 'Bishop',
      health: 50,
      movement: 1,
      player: null,
      physicalDefence: 2,
      magicalDefence: 10,
      range: 1,
      damage: 30,
      movementToken: 1,
      damageType: 'mag',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  })).toEqual(30)
})

it('resets the hasMoved, hasAttacked and movementToken values back to their default', () => {
  wrapper.vm.info={
    image: "crossed-swords",
    archetype: {
      name: 'Soldier',
      health: 80,
      movement: 2,
      player: null,
      physicalDefence: 10,
      magicalDefence: 4,
      range: 1,
      damage: 27,
      movementToken: 0,
      damageType: 'phys',
    },
    hasMoved: true,
    style: 'border:1px solid black',
    hasAttacked: true
  }
  wrapper.vm.endTurn()
  expect(wrapper.vm.info.hasMoved).toBe(false)
  expect(wrapper.vm.info.hasAttacked).toBe(false)
  expect(wrapper.vm.info.archetype.movementToken).toEqual(2)
})
})
